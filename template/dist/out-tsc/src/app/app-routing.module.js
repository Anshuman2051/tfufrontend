"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var header_component_1 = require("./header/header.component");
var feedback_component_1 = require("./feedback/feedback.component");
var gen_instructions_component_1 = require("./gen-instructions/gen-instructions.component");
var test_insrtructions_component_1 = require("./test-insrtructions/test-insrtructions.component");
var test_page_component_1 = require("./test-page/test-page.component");
var result_summary_component_1 = require("./result-summary/result-summary.component");
var page_not_found_component_1 = require("./page-not-found/page-not-found.component");
var routes = [
    { path: "", component: header_component_1.HeaderComponent },
    { path: "general_instructions", component: gen_instructions_component_1.GenInstructionsComponent },
    { path: "test-instructions", component: test_insrtructions_component_1.TestInsrtructionsComponent },
    { path: "test-page", component: test_page_component_1.TestPageComponent },
    { path: "result", component: result_summary_component_1.ResultSummaryComponent },
    { path: "dashboard", loadChildren: "./after-login/after-login.module#AfterLoginModule" },
    { path: "**", component: page_not_found_component_1.PageNotFoundComponent }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forRoot(routes, { enableTracing: true })],
            exports: [router_1.RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;
exports.importedComponents = [header_component_1.HeaderComponent, feedback_component_1.FeedbackComponent, gen_instructions_component_1.GenInstructionsComponent,
    test_insrtructions_component_1.TestInsrtructionsComponent, test_page_component_1.TestPageComponent, result_summary_component_1.ResultSummaryComponent];
//# sourceMappingURL=app-routing.module.js.map