"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.toogleButton = document.getElementsByClassName('toogle_button');
        this.side = document.getElementsByClassName('sideNav'); //side nav bar
        this.navList = document.getElementsByClassName('nav_list'); //top navlist
    }
    ; //bars on top right
    AppComponent.prototype.onClick = function () {
        console.log(this.side);
        this.toogleButton[0].style.display = "none";
        this.side[0].style.display = "block";
    };
    AppComponent.prototype.closed = function () {
        this.side[0].style.display = "none";
        this.toogleButton[0].style.display = "block";
    };
    AppComponent.prototype.switchingRootSideNav = function () {
        var _this = this;
        if (innerWidth > 820) {
            this.toogleButton[0].style.display = 'none';
            this.navList[0].style.display = "block";
        }
        else {
            this.navList[0].style.display = "none";
            this.toogleButton[0].style.display = 'block';
        }
        window.onresize = function () {
            if (innerWidth > 820) {
                _this.toogleButton[0].style.display = 'none';
                _this.navList[0].style.display = "block";
                _this.side[0].style.display = "none";
            }
            else {
                _this.toogleButton[0].style.display = 'block';
                _this.navList[0].style.display = "none";
            }
        };
    };
    AppComponent.prototype.ngOnInit = function () {
        this.switchingRootSideNav();
    };
    AppComponent.prototype.ngAfterViewInit = function () {
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.scss']
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map