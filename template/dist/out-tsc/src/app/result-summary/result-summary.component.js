"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var test_data_service_1 = require("../test-data.service");
var ResultSummaryComponent = /** @class */ (function () {
    function ResultSummaryComponent(testDataService) {
        this.testDataService = testDataService;
    }
    ResultSummaryComponent.prototype.getData = function () {
        var _this = this;
        this.testDataService.getTestData().subscribe(function (paraData) { return _this.ques = paraData; });
    };
    //  giving response
    ResultSummaryComponent.prototype.response = function (i) {
        if (this.ques[i].response == null || !this.ques[i].response.localeCompare('-1'))
            return 'nill';
        else
            return '' + (1 + Number(this.ques[i].response));
    };
    // matching response with answer
    ResultSummaryComponent.prototype.match = function (i) {
        if (this.ques[i].response == null)
            return -1;
        else if (Number(this.ques[i].response) + 1 == (Number(this.ques[i].answer))) {
            console.log("true");
            return 1;
        }
        else
            return 0;
    };
    ResultSummaryComponent.prototype.ngOnInit = function () {
        this.getData();
    };
    ResultSummaryComponent = __decorate([
        core_1.Component({
            selector: 'app-result-summary',
            templateUrl: './result-summary.component.html',
            styleUrls: ['./result-summary.component.scss']
        }),
        __metadata("design:paramtypes", [test_data_service_1.TestDataService])
    ], ResultSummaryComponent);
    return ResultSummaryComponent;
}());
exports.ResultSummaryComponent = ResultSummaryComponent;
//# sourceMappingURL=result-summary.component.js.map