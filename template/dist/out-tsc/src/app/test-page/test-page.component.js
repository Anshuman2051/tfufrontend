"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var test_data_service_1 = require("../test-data.service");
var TestPageComponent = /** @class */ (function () {
    function TestPageComponent(testDataService) {
        this.testDataService = testDataService;
        this.tempResponse = null;
        this.index = 0;
    }
    TestPageComponent.prototype.getData = function () {
        var _this = this;
        this.testDataService.getTestData().subscribe(function (paraData) { return _this.ques = paraData; });
    };
    // saving the response by user
    TestPageComponent.prototype.saveResponse = function (index) {
        if (this.tempResponse == null) {
            alert("please enter a response");
            return;
        }
        this.ques[index].response = "" + (this.tempResponse);
        this.changeClassName(this.index, "answered");
        this.tempResponse = null;
        console.log(this.ques[index]);
        if (this.ques[index].marked_review == true) {
            this.ques[index].marked_review = false;
        }
        this.showQuestion(this.index + 1);
    };
    // clear button event
    TestPageComponent.prototype.onClearOption = function () {
        var c = document.getElementsByName('singleCorrect');
        for (var i = 0; i < c.length; i++) {
            c[i].checked = false;
        }
        this.ques[this.index].response = null;
        this.ques[this.index].marked_review = false;
        console.log(this.ques[this.index]);
        this.changeClassName(this.index, "not_answered");
    };
    // click of marked for review
    TestPageComponent.prototype.markedReview = function (index) {
        this.ques[index].marked_review = true;
        if (this.tempResponse == null) {
            this.tempResponse = -1;
            this.changeClassName(this.index, "marked");
        }
        else
            this.changeClassName(this.index, "marked_answered");
        this.ques[index].response = "" + (this.tempResponse);
        this.tempResponse = null;
        console.log(this.ques[index]);
        this.showQuestion(this.index + 1);
    };
    // shows question on clicking in the side nav
    TestPageComponent.prototype.showQuestion = function (index) {
        this.index = index;
        if (this.ques[index].response == null) {
            this.changeClassName(this.index, "not_answered");
        }
        else if (this.ques[index].response.localeCompare("-1") == 0) {
            this.changeClassName(this.index, "marked");
        }
        else if (this.ques[index].marked_review) {
            this.changeClassName(this.index, "marked_answered");
        }
        else
            this.changeClassName(this.index, "answered");
    };
    TestPageComponent.prototype.changeClassName = function (index, className) {
        this.quesNo = document.getElementsByClassName('ques_no');
        this.quesNo[index].className = "col-2 ques_no" + " " + className;
    };
    // open button clicked
    TestPageComponent.prototype.toogle = function (status) {
        var btn = document.getElementById("open_button");
        var sideNav = document.getElementById('side_nav');
        if (status) {
            btn.style.display = "none";
            sideNav.style.display = "block";
        }
        else {
            btn.style.display = "block";
            sideNav.style.display = "none";
        }
    };
    // on test completion
    TestPageComponent.prototype.testComplete = function () {
        if (confirm("do you want to finish the test? ")) {
            alert("you r foolish aren`t u?");
        }
    };
    // switchingSideNav(){
    //   let btn = document.getElementById("open_button");
    //   let sideNav = document.getElementById('side_nav'); 
    //   window.onresize = ()=>{
    //    if(innerWidth>768){
    //     btn.style.display = 'none';
    //     sideNav.style.display='block';}
    //    else {
    //      btn.style.display = 'block';
    //      sideNav.style.display='none';
    //     }
    //       }
    // }
    TestPageComponent.prototype.ngOnInit = function () {
        this.getData();
        this.res = new ResponseCount(this.ques.length - 1);
        var time = 30 * 60;
        var display = document.querySelector('#timer');
        var setTimer = new Countdowntimer();
        setTimer.startTimer(time, display);
        // this.switchingSideNav();
    };
    TestPageComponent.prototype.ngAfterViewInit = function () {
        this.changeClassName(0, "not_answered");
    };
    TestPageComponent = __decorate([
        core_1.Component({
            selector: 'app-test-page',
            templateUrl: './test-page.component.html',
            styleUrls: ['./test-page.component.scss']
        }),
        __metadata("design:paramtypes", [test_data_service_1.TestDataService])
    ], TestPageComponent);
    return TestPageComponent;
}());
exports.TestPageComponent = TestPageComponent;
var Countdowntimer = /** @class */ (function () {
    function Countdowntimer() {
    }
    // countdown timer
    Countdowntimer.prototype.startTimer = function (duration, display) {
        var timer = duration, hours, minutes, seconds;
        setInterval(function () {
            hours = Math.floor(timer / 3600);
            minutes = Math.floor((timer % 3600) / 60);
            seconds = Math.floor(timer % 60);
            hours = hours < 10 ? "0" + hours : hours;
            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;
            display.textContent = hours + ":" + minutes + ":" + seconds;
            if (--timer < 0) {
                this.testComplete();
            }
        }, 1000);
    };
    return Countdowntimer;
}());
var ResponseCount = /** @class */ (function () {
    function ResponseCount(count_not_visited) {
        // constructor for not visited required for accessing number of not visited
        this.count_answered = 0;
        this.count_marked = 0;
        this.count_marked_answered = 0;
        this.count_not_answered = 1;
        this.count_not_visited = 0;
        this.count_not_visited = count_not_visited;
    }
    // for counting questions for summary list
    ResponseCount.prototype.summaryCount = function () {
        console.log("on it");
        var answered = document.getElementsByClassName('answered');
        var notAnswered = document.getElementsByClassName('not_answered');
        var marked = document.getElementsByClassName('marked');
        var markedAnswered = document.getElementsByClassName('marked_answered');
        var not_visited = document.getElementsByClassName('not_visited');
        this.count_answered = answered.length - 1;
        this.count_not_answered = notAnswered.length - 1;
        this.count_marked = marked.length - 1;
        this.count_marked_answered = markedAnswered.length - 1;
        this.count_not_visited = not_visited.length - 1;
    };
    return ResponseCount;
}());
//# sourceMappingURL=test-page.component.js.map