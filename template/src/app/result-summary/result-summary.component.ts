import { Component, OnInit } from '@angular/core';
import { testDataFormat } from '../testDataFormat';
import { TestDataService } from '../test-data.service';
import { TestPageComponent } from '../test-page/test-page.component';

@Component({
  selector: 'app-result-summary',
  templateUrl: './result-summary.component.html',
  styleUrls: ['./result-summary.component.scss']
})
export class ResultSummaryComponent implements OnInit {
  ques: testDataFormat[];

  getData():void{
    this.testDataService.getTestData().subscribe(
      (paraData)=>this.ques = paraData
    );
  }


  //  giving response
 response(i:number):string{
   if(this.ques[i].response == null || !this.ques[i].response.localeCompare('-1'))
   return 'nill';
   else return ''+(1+Number(this.ques[i].response));
 }

// matching response with answer
match(i:number):number{
  if(this.ques[i].response == null)
  return -1;
  else if(Number(this.ques[i].response)+1==(Number(this.ques[i].answer))){
    console.log("true");return 1;}
  else
  return 0;
}
  constructor(private testDataService:TestDataService) { }

  ngOnInit() {
    this.getData();
  }

}
