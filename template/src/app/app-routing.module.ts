import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeaderComponent }from './header/header.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { GenInstructionsComponent } from './gen-instructions/gen-instructions.component';
import { TestInsrtructionsComponent } from './test-insrtructions/test-insrtructions.component';
import { TestPageComponent } from './test-page/test-page.component';
import { ResultSummaryComponent } from './result-summary/result-summary.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import{ DashComponent} from './dash/dash.component';

const routes: Routes =[
  { path:"", component:HeaderComponent},
  { path:"dashboard", loadChildren:"app/after-login/after-login.module#AfterLoginModule" },
  { path:"general_instructions", component:GenInstructionsComponent},
  { path:"test-instructions", component:TestInsrtructionsComponent},
  { path:"test-page", component:TestPageComponent},
  { path:"result",component:ResultSummaryComponent},
  { path:"dash", component:DashComponent},
  { path:"**", component:PageNotFoundComponent }
];


@NgModule({
    imports: [ RouterModule.forRoot(routes,
    {enableTracing:true } )],   //for debugging
    exports: [ RouterModule ]
  })
 export class AppRoutingModule {}
 export const importedComponents = [HeaderComponent, FeedbackComponent, GenInstructionsComponent,
   TestInsrtructionsComponent, TestPageComponent, ResultSummaryComponent]