import { Injectable } from '@angular/core';
import { question } from './mockTestData';
import { testDataFormat } from './testDataFormat';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';



@Injectable()
export class TestDataService {

  constructor() { }
  getTestData():Observable<testDataFormat[]>{
    console.log(" :) fetched data from mock test data ;)");
    return of(question);
  }
  

}
