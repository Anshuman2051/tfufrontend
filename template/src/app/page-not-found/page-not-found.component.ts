import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-not-found',
  template: `
    <p class="h3">
      sorry page-not-found <br>
    </p>
    
  `,
  styles: [
    `p{margin-top:100px;}`
  ]
})
export class PageNotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
