import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestInsrtructionsComponent } from './test-insrtructions.component';

describe('TestInsrtructionsComponent', () => {
  let component: TestInsrtructionsComponent;
  let fixture: ComponentFixture<TestInsrtructionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestInsrtructionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestInsrtructionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
