import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dash',
  templateUrl: './dash.component.html',
  styleUrls: ['./dash.component.scss']
})
export class DashComponent implements OnInit {
  toogleButton = document.getElementsByClassName('toogle_button') as HTMLCollectionOf<HTMLInputElement>; ;  //bars on top right
  side = document.getElementsByClassName('sideNav') as HTMLCollectionOf<HTMLInputElement>;               //side nav bar
  navList = document.getElementsByClassName('nav_list') as HTMLCollectionOf<HTMLInputElement>;           //top navlist
 onClick():void{
   console.log(this.side);
   this.toogleButton[0].style.display="none";
   this.side[0].style.display="block";
  }
  closed():void{
   this.side[0].style.display="none";
   this.toogleButton[0].style.display="block";
  }

  switchingRootSideNav(){
    if(innerWidth>820){
     this.toogleButton[0].style.display = 'none';
     this.navList[0].style.display="block";
    }
    else{
     this.navList[0].style.display="none";
     this.toogleButton[0].style.display = 'block';
    }
    window.onresize = ()=>{
     if(innerWidth>820){
      this.toogleButton[0].style.display = 'none';
      this.navList[0].style.display="block";
      this.side[0].style.display="none";
     }
     else {
       this.toogleButton[0].style.display = 'block';
       this.navList[0].style.display="none";
   }
        }
  }


  constructor() { }

  ngOnInit() {
    this.switchingRootSideNav();
  }

}
