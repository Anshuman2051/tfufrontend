import { Component, OnInit } from '@angular/core';
import { question } from '../mockTestData';
import { testDataFormat } from '../testDataFormat';
import { TestDataService } from '../test-data.service';

 @Component({
  selector: 'app-test-page',
  templateUrl: './test-page.component.html',
  styleUrls: ['./test-page.component.scss']
 })
export class TestPageComponent implements OnInit { 
   ques: testDataFormat[]; //contains data from question
   res:ResponseCount; //response class var
  getData():void{
    this.testDataService.getTestData().subscribe(
      (paraData)=>this.ques = paraData
    );
  }

  constructor(private testDataService:TestDataService) { }

  tempResponse:number=null;
  index:number=0;
  // saving the response by user
  saveResponse(index:number):void{
    if( this.tempResponse==null){
      alert("please enter a response");
      return;
     }
    this.ques[index].response=""+(this.tempResponse);
    this.changeClassName(this.index,"answered");
    this.tempResponse=null;
    console.log(this.ques[index]);

    if(this.ques[index].marked_review==true){
      this.ques[index].marked_review=false;
      }
    this.showQuestion(this.index+1);
  }
  
  // clear button event
  onClearOption(){
    var c=document.getElementsByName('singleCorrect') as HTMLCollectionOf<HTMLInputElement>;
    for(let i=0 ; i< c.length; i++){
        c[i].checked = false;
        }
    this.ques[this.index].response=null;
    this.ques[this.index].marked_review=false;
    console.log(this.ques[this.index]);
    this.changeClassName(this.index,"not_answered");
    }
  // click of marked for review
  markedReview(index:number):void{
    this.ques[index].marked_review=true;
    if(this.tempResponse==null){
      this.tempResponse=-1;
      this.changeClassName(this.index,"marked");
    }
     else this.changeClassName(this.index,"marked_answered");
  

     this.ques[index].response=""+(this.tempResponse);
     this.tempResponse=null;
     console.log(this.ques[index]);
      this.showQuestion(this.index+1);
  }

  // shows question on clicking in the side nav
  showQuestion(index:number):void{  
    this.index = index;
    if(this.ques[index].response == null){
      this.changeClassName(this.index,"not_answered");}
    else if(this.ques[index].response.localeCompare("-1")==0){
      this.changeClassName(this.index,"marked"); 
    }
    else if(this.ques[index].marked_review) {
      this.changeClassName(this.index,"marked_answered");
    }
    else  this.changeClassName(this.index,"answered");
  }
  // changing the class names 
  quesNo:object;
  changeClassName(index:number ,className:string):void {
    this.quesNo = document.getElementsByClassName('ques_no') as HTMLCollectionOf<HTMLElement>;
    this.quesNo[index].className="col-2 ques_no"+" "+className;
  }  

  // open button clicked
  toogle(status:boolean):void{
   let btn = document.getElementById("open_button");
   let sideNav = document.getElementById('side_nav');
   if(status){
    btn.style.display ="none";
    sideNav.style.display ="block";
    }
   else{
    btn.style.display ="block";
    sideNav.style.display ="none";
   }
  }
  // on test completion
  testComplete():void{
    if(confirm("do you want to finish the test? ")){
      alert("you r foolish aren`t u?");
    }
  }

  // switchingSideNav(){
  //   let btn = document.getElementById("open_button");
  //   let sideNav = document.getElementById('side_nav'); 
  //   window.onresize = ()=>{
  //    if(innerWidth>768){
  //     btn.style.display = 'none';
  //     sideNav.style.display='block';}
  //    else {
  //      btn.style.display = 'block';
  //      sideNav.style.display='none';
  //     }
  //       }
  // }

  ngOnInit() {
    this.getData();
    this.res = new ResponseCount(this.ques.length-1);
    var time =30*60;
    var display = document.querySelector('#timer');
    var setTimer = new Countdowntimer();
    setTimer.startTimer(time,display);
    // this.switchingSideNav();
  }

  ngAfterViewInit(){
    this.changeClassName(0,"not_answered");  
  }

}

class Countdowntimer{

    // countdown timer
 startTimer(duration, display) 
 {
    var timer = duration,hours, minutes, seconds;
    setInterval(function () {
        hours = Math.floor(timer/3600);
        minutes = Math.floor((timer%3600)/60);
        seconds = Math.floor(timer % 60);

        hours = hours < 10 ? "0" + hours : hours;
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = hours + ":" +minutes + ":" + seconds;

        if (--timer < 0) {
            this.testComplete();
        }
    }, 1000);
  }


}

class ResponseCount{
  // constructor for not visited required for accessing number of not visited
  count_answered:number =0;
  count_marked:number = 0;
  count_marked_answered:number =0;
  count_not_answered:number= 1;
  count_not_visited:number=0;
  constructor(count_not_visited:number){
    this.count_not_visited = count_not_visited;
  }
  // for counting questions for summary list
  summaryCount():void{
    console.log("on it");
    let answered =document.getElementsByClassName('answered') as HTMLCollectionOf<HTMLElement>;
    let notAnswered = document.getElementsByClassName('not_answered') as HTMLCollectionOf<HTMLElement>;
    let marked = document.getElementsByClassName('marked') as HTMLCollectionOf<HTMLElement>;
    let markedAnswered= document.getElementsByClassName('marked_answered') as HTMLCollectionOf<HTMLElement>;
    let not_visited = document.getElementsByClassName('not_visited') as HTMLCollectionOf<HTMLElement>;
    this.count_answered = answered.length-1;
    this.count_not_answered = notAnswered.length-1; 
    this.count_marked = marked.length-1;
    this.count_marked_answered = markedAnswered.length -1;
    this.count_not_visited = not_visited.length-1;
  }

}