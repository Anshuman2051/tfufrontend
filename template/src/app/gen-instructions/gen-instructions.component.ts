import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gen-instructions',
  templateUrl: './gen-instructions.component.html',
  styleUrls: ['./gen-instructions.component.scss']
})
export class GenInstructionsComponent implements OnInit {
  btn= document.getElementsByClassName('next-btn') ;
  checkBoxClicked(e){
    if(!e.target.checked ){
      this.btn[0].setAttribute("disabled","true");
  }
  else this.btn[0].removeAttribute("disabled");
  }

  constructor() { }

  ngOnInit() {
  }

}
