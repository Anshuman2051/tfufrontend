import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public studentsClicked:boolean = true;  
  public studentsWorkClicked =true;
  constructor() { }

  ngOnInit() {
    
  }
  btnClicked(input:boolean):void{
    this.studentsClicked = input;
  }
  btnWorkClicked(input:boolean):void{
    this.studentsWorkClicked = input;
  }
  

}
